FROM node:lts

WORKDIR /src

ENV NODE_ENV production

COPY app/. ./src

CMD [ "node", "server.js" ]
